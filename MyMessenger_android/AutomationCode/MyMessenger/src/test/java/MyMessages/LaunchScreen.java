package MyMessages;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URLConnection;
import java.net.URL;
import java.util.HashMap;

import com.rohit.asap.CommonFunctions;
import com.rohit.asap.Global;
import com.rohit.asap.Reporting;

public class LaunchScreen {

	private Reporting Reporter;
	private HashMap<String, String> Dictionary;
	private CommonFunctions objCommon = new CommonFunctions();

	// Define the constructor
	public LaunchScreen() {
		Reporter = Global.Reporter;
		Dictionary = Global.Dictionary;
	}

	public String MyMessengerText = "xpath:=//android.widget.TextView[@text='My Messenger']";
	public String AppIconView = "xpath:=//android.widget.ImageView[@index='0']";
	public String WelcomeMessage = "xpath:=//android.widget.TextView[@text='Welcome to Messenger app!']";
	public String ALREADYHAVEACCOUNTBTN = "xpath:=//android.widget.Button[@text='ALREADY HAVE ACCOUNT']";
	public String NEEDNEWACCOUNTBTN = "xpath:=//android.widget.Button[@text='NEED A NEW ACCOUNT?']";

	public boolean Launch() {
		if (LaunchscreenUI() == true) {
			Reporter.fnWriteToHtmlOutput("Look and feel of launch screen", "Launch screen UI",
					"Launch screen is appearing and its UI is as expected", "Pass");
		} else {
			Reporter.fnWriteToHtmlOutput("Look and feel of launch screen", "Launch screen UI",
					"Launch screen is appearing but its UI is not as expected", "Fail");
			return false;
		}

		return true;
	}

	public boolean LaunchscreenUI() {
		System.out.println("Into Launch screen - Identifying UI components of login screen");
		// My messenger text
		if (objCommon.fGuiIsDisplayed(MyMessengerText) == false) {
			return false;
		}
		// App icon identification
		if (objCommon.fGuiIsDisplayed(AppIconView) == false) {
			return false;
		}
		// Welcome message text
		if (objCommon.fGuiIsDisplayed(WelcomeMessage) == false) {
			return false;
		}
		// Already have account button
		if (objCommon.fGuiIsDisplayed(ALREADYHAVEACCOUNTBTN) == false) {
			return false;
		}
		// Need new account button
		if (objCommon.fGuiIsDisplayed(NEEDNEWACCOUNTBTN) == false) {
			return false;
		}

		return true;
	}

}
