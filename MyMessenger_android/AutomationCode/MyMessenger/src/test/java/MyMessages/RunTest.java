package MyMessages;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import com.rohit.asap.CommonFunctions;
import com.rohit.asap.Driver;
import com.rohit.asap.Global;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import io.appium.java_client.android.AndroidDriver;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class RunTest {

	// Variables
	String className;
	String dataSheetName;
	String env;
	String buildNumber;
	String jobName;

	// Instances
	Driver asapDriver;
	WebDriver driver;
	CommonFunctions objCommon;

	@BeforeClass
	public void beforeClass() throws IOException {
		System.out.println("Before Class TestsForDemo");
		// Set the DataSheet name by getting the class name
		String[] strClassNameArray = this.getClass().getName().split("\\.");
		className = strClassNameArray[strClassNameArray.length - 1];
		Global.Environment.put("CLASSNAME", className);
		// Initiate asapDriver
		asapDriver = new Driver();
		// Check if POM has env, if null, get it from config file
		if (System.getProperty("envName") == null)
			env = asapDriver.fGetEnv();
		else
			env = System.getProperty("envName");
		// Add env global environments
		Global.Environment.put("ENV_CODE", env);
		// Create folder structure
		asapDriver.createExecutionFolders();
		// Get Environment Variables
		asapDriver.fetchEnvironmentDetails();
		// Create HTML Summary Report
		Global.Reporter.fnCreateSummaryReport();
		// Initiate WebDriver
		Global.androidDriver = (AndroidDriver) asapDriver.fGetWebDriver();
		driver = Global.androidDriver;
		// Set implicit time
		if (driver != null)
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		// Initialize Common functions
		objCommon = new CommonFunctions();
	}

	@BeforeMethod
	public void beforeMethod(Method method) {
		// Get the test name
		String testName = method.getName();
		System.out.println("Before Method" + testName);
		// Get the data from DataSheet corresponding to Class Name & Test Name
		asapDriver.fGetDataForTest(testName);
		// Create Individual HTML Report
		Global.Reporter.fnCreateHtmlReport(testName);
	}

	@Test
	public void EntireTest() {
		System.out.println("Into Launch");
		LaunchScreen Option = new LaunchScreen();
		Option.Launch();
	
		System.out.println("Into Registration");
		RegistrationScreen Reg = new RegistrationScreen();
		Reg.Registration();
	

		System.out.println("Into Login");
		LoginScreen Log = new LoginScreen();
		Log.Login();
	}
/*	
	@Test 
	public void Launch() {
	 	LaunchScreen Option = new LaunchScreen();
	 	Option.Launch();
	} 
	
	@Test 
	public void
	RegistrationTest() { 
		RegistrationScreen Reg = new RegistrationScreen();
		Reg.Registration(); 
	}
	
	@Test
	public void LoginScreenTest() { 
		LoginScreen Log = new LoginScreen();
	    Log.Login(); 
	}
 */

	@AfterMethod
	public void afterMethod(Method method) {

		// Get the test name
		String testName = method.getName();

		System.out.println("After Method" + testName);

		// Update the KeepRefer Sheet
		asapDriver.fSetReferenceData();

		// Close Individual Summary Report & Update Summary Report
		Global.Reporter.fnCloseHtmlReport(testName);

	}

	@AfterClass
	public void afterClass() {
		System.out.println("After Class TestsForAppium");

		// Close HTML Summary report
		Global.Reporter.fnCloseTestSummary();

		// QUit webdriver
		if (Global.androidDriver != null)
			Global.androidDriver.quit();
	}

}
