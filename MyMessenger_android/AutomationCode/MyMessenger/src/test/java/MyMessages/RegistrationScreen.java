package MyMessages;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URLConnection;
import java.net.URL;
import java.util.HashMap;

import com.rohit.asap.CommonFunctions;
import com.rohit.asap.Global;
import com.rohit.asap.Reporting;

public class RegistrationScreen {

	private Reporting Reporter;
	private HashMap<String, String> Dictionary;
	private CommonFunctions objCommon = new CommonFunctions();

	// Define the constructor
	public RegistrationScreen() {
		Reporter = Global.Reporter;
		Dictionary = Global.Dictionary;
	}

	public String NEEDNEWACCOUNTBTN = "xpath:=//android.widget.Button[@text='NEED A NEW ACCOUNT?']";
	public String MyMessengerText = "xpath:=//android.view.ViewGroup//android.widget.TextView[@index='0']";
	public String Creatanewaccount = "xpath:=//android.widget.TextView[@text='Create a new account']";
	public String Enterdisplayname = "xpath:=//android.widget.LinearLayout//android.widget.EditText[@index='2']";
	public String Enteremail = "xpath:=//android.widget.LinearLayout//android.widget.EditText[@index='3']";
	public String Enterpassword = "xpath:=//android.widget.LinearLayout//android.widget.EditText[@index='4']";
	public String CREATEACCOUNT = "xpath:=//android.widget.Button[@text='CREATE ACCOUNT']";
	public String DASHBOARD = "xpath:=//android.widget.TextView[@text='Dashboard']";
	public String MoreOption = "xpath:=//android.widget.FrameLayout//android.widget.ImageView[@index='0']";
	public String Logout = "xpath:=//android.widget.TextView[@text='Logout']";

	public boolean Registration() {
		if (RegistrationScreenUI() == true) {
			Reporter.fnWriteToHtmlOutput("Look and feel of Registration screen", "Registration screen UI",
					"Registration screen is appearing and its UI is as expected", "Pass");
		} else {
			Reporter.fnWriteToHtmlOutput("Look and feel of Registration screen", "Registration screen UI",
					"Registration screen is appearing but its UI is not as expected", "Fail");
			return false;
		}

		if (CreateAccountError() == true) {
			Reporter.fnWriteToHtmlOutput(
					"Error scenarios - Create account with empty inputs, invalid email or password inputs",
					"Error occurs while creating account with empty inputs, invalid email or password inputs",
					"App shows error while creating account with empty inputs, invalid email or password inputs",
					"Pass");
		} else {
			Reporter.fnWriteToHtmlOutput(
					"Error scenarios - Create account with empty inputs, invalid email or password inputs",
					"Error occurs while creating account with empty inputs, invalid email or password inputs",
					"App does not show error while creating account with empty inputs, invalid email or password inputs",
					"Fail");
			return false;
		}

		if (CreateAccount() == true) {
			Reporter.fnWriteToHtmlOutput("AccountCreatedSuccessfully", "App creates new account successfully' button",
					"App creates new account and navigates to Dashboard screen", "Pass");
		} else {
			Reporter.fnWriteToHtmlOutput("AccountCreatedSuccessfully", "App creates new account successfully' button",
					"App does not create new account", "Fail");
			return false;
		}

		if (Logout() == true) {
			Reporter.fnWriteToHtmlOutput("Logout from app", "Logout sucessful", "App logsout successful", "Pass");
		} else {
			Reporter.fnWriteToHtmlOutput("Logout from app", "Logout sucessful", "App logsout successful", "Fail");
			return false;
		}

		return true;
	}

	public boolean RegistrationScreenUI() {
		System.out.println("Into Registration screen");
		if (objCommon.fGuiClick(NEEDNEWACCOUNTBTN) == false) {
			return false;
		}
		System.out.println("Identifying UI components of Registration screen");
		// My messenger text
		if (objCommon.fGuiIsDisplayed(MyMessengerText) == false) {
			return false;
		}
		// Create a new account text
		if (objCommon.fGuiIsDisplayed(Creatanewaccount) == false) {
			return false;
		}
		// Enter display name text
		if (objCommon.fGuiIsDisplayed(Enterdisplayname) == false) {
			return false;
		}
		// Enter password text
		if (objCommon.fGuiIsDisplayed(Enterpassword) == false) {
			return false;
		}
		// Create account button
		if (objCommon.fGuiIsDisplayed(CREATEACCOUNT) == false) {
			return false;
		}

		return true;
	}

	public boolean CreateAccountError() {

		System.out.println("Error while clicking Create Account button without adding any input");
		// Click CREATE ACCOUNT button
		if (objCommon.fGuiClick(CREATEACCOUNT) == false) {
			return false;
		}
		System.out.println("Remains on registration screen only");
		if (objCommon.fGuiIsDisplayed(Creatanewaccount) == false) {
			return false;
		}

		System.out.println("Invalid email input");

		// Click and enter display name
		if (objCommon.fGuiClick(Enterdisplayname) == false) {
			return false;
		}
		if (objCommon.fGuiEnterText(Enterdisplayname, Dictionary.get("NAMEREG")) == false) {
			return false;
		}
		// Hide keyboard
		if (objCommon.fGuiHideKeyBoard() == false) {
			return false;
		}
		// Click and enter email name
		if (objCommon.fGuiClick(Enteremail) == false) {
			return false;
		}
		if (objCommon.fGuiEnterText(Enteremail, Dictionary.get("REGINVALIDEMAIL")) == false) {
			return false;
		}
		// Hide keyboard
		if (objCommon.fGuiHideKeyBoard() == false) {
			return false;
		}
		// Click and enter password
		if (objCommon.fGuiClick(Enterpassword) == false) {
			return false;
		}
		if (objCommon.fGuiEnterText(Enterpassword, Dictionary.get("PASSWORDREG")) == false) {
			return false;
		}
		// Hide keyboard
		if (objCommon.fGuiHideKeyBoard() == false) {
			return false;
		}
		// Click CREATE ACCOUNT button
		if (objCommon.fGuiClick(CREATEACCOUNT) == false) {
			return false;
		}
		System.out.println("Remains on registration screen only");
		if (objCommon.fGuiIsDisplayed(Creatanewaccount) == false) {
			return false;
		}

		System.out.println("Error occurs, if password length is less than 6");

		// Click and enter email name
		if (objCommon.fGuiClick(Enteremail) == false) {
			return false;
		}
		if (objCommon.fGuiEnterText(Enteremail, Dictionary.get("EMAILREG")) == false) {
			return false;
		}
		// Hide keyboard
		if (objCommon.fGuiHideKeyBoard() == false) {
			return false;
		}
		// Click and enter password
		if (objCommon.fGuiClick(Enterpassword) == false) {
			return false;
		}
		if (objCommon.fGuiEnterText(Enterpassword, Dictionary.get("REGINVALIDPASSWORD")) == false) {
			return false;
		}
		// Hide keyboard
		if (objCommon.fGuiHideKeyBoard() == false) {
			return false;
		}
		// Click CREATE ACCOUNT button
		if (objCommon.fGuiClick(CREATEACCOUNT) == false) {
			return false;
		}
		System.out.println("Remains on registration screen only");
		if (objCommon.fGuiIsDisplayed(Creatanewaccount) == false) {
			return false;
		}
		return true;
	}

	public boolean CreateAccount() {

		System.out.println("Fill up the registration form to create account");
		// Click and enter display name
		if (objCommon.fGuiClick(Enterdisplayname) == false) {
			return false;
		}
		if (objCommon.fGuiEnterText(Enterdisplayname, Dictionary.get("NAMEREG")) == false) {
			return false;
		}
		// Hide keyboard
		if (objCommon.fGuiHideKeyBoard() == false) {
			return false;
		}
		// Click and enter email name
		if (objCommon.fGuiClick(Enteremail) == false) {
			return false;
		}
		if (objCommon.fGuiEnterText(Enteremail, Dictionary.get("EMAILREG")) == false) {
			return false;
		}
		// Hide keyboard
		if (objCommon.fGuiHideKeyBoard() == false) {
			return false;
		}
		// Click and enter password
		if (objCommon.fGuiClick(Enterpassword) == false) {
			return false;
		}
		if (objCommon.fGuiEnterText(Enterpassword, Dictionary.get("PASSWORDREG")) == false) {
			return false;
		}
		// Hide keyboard
		if (objCommon.fGuiHideKeyBoard() == false) {
			return false;
		}
		// Click CREATE ACCOUNT button
		if (objCommon.fGuiClick(CREATEACCOUNT) == false) {
			return false;
		}
		try {
			Thread.sleep(7000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.print("Into home screen");
		// Dashboard text on home screen
		if (objCommon.fGuiClick(DASHBOARD) == false) {
			return false;
		}

		return true;
	}

	public boolean Logout() {

		if (objCommon.fGuiClick(MoreOption) == false) {
			return false;
		}

		if (objCommon.fGuiClick(Logout) == false) {
			return false;
		}

		return true;
	}

}
