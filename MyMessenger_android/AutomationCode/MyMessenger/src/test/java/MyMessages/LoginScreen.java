package MyMessages;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URLConnection;
import java.net.URL;
import java.util.HashMap;

import com.rohit.asap.CommonFunctions;
import com.rohit.asap.Global;
import com.rohit.asap.Reporting;

public class LoginScreen {

	private Reporting Reporter;
	private HashMap<String, String> Dictionary;
	private CommonFunctions objCommon = new CommonFunctions();

	// Define the constructor
	public LoginScreen() {
		Reporter = Global.Reporter;
		Dictionary = Global.Dictionary;
	}

	public String ALREADYHAVEACCOUNTBTN = "xpath:=//android.widget.Button[@text='ALREADY HAVE ACCOUNT']";
	public String AlreadyhaveanaccountText = "xpath:=//android.widget.TextView[@text='Already have an account?']";
	public String Enteremail = "xpath:=//android.widget.LinearLayout//android.widget.EditText[@index='1']";
	public String Enterpassword = "xpath:=//android.widget.LinearLayout//android.widget.EditText[@index='2']";
	public String LOGINBTN = "xpath:=//android.widget.Button[@text='LOGIN']";
	public String DASHBOARD = "xpath:=//android.widget.TextView[@text='Dashboard']";
	public String MoreOption = "xpath:=//android.widget.FrameLayout//android.widget.ImageView[@index='0']";
	public String Logout = "xpath:=//android.widget.TextView[@text='Logout']";

	public boolean Login() {
		if (LoginUI() == true) {
			Reporter.fnWriteToHtmlOutput("Look and feel of Login screen", "Login screen UI",
					"Login screen is appearing and its UI is as expected", "Pass");
		} else {
			Reporter.fnWriteToHtmlOutput("Look and feel of Login screen", "Login screen UI",
					"Login screen is appearing but its UI is not as expected", "Fail");
			return false;
		}

		if (LoginError() == true) {
			Reporter.fnWriteToHtmlOutput("Error scenarios - Login with empty inputs, invalid credentials",
					"Error occurs while Login with empty inputs, invalid credentials",
					"App shows error while Login with empty inputs, invalid credentials", "Pass");
		} else {
			Reporter.fnWriteToHtmlOutput("Error scenarios - Login with empty inputs, invalid credentials",
					"Error occurs while Login with empty inputs, invalid credentials",
					"App does not show error while Login with empty inputs, invalid credentials", "Fail");
			return false;
		}

		if (SuccessfulLogin() == true) {
			Reporter.fnWriteToHtmlOutput("Successful Login", "Successful Login with valid credentials",
					"Login successful and Home screen gets appearred", "Pass");
		} else {
			Reporter.fnWriteToHtmlOutput("Successful Login", "Successful Login with valid credentials",
					"Login unsuccessful", "Fail");
			return false;
		}

		if (Logout() == true) {
			Reporter.fnWriteToHtmlOutput("Logout from app", "Logout sucessful", "App logsout successful", "Pass");
		} else {
			Reporter.fnWriteToHtmlOutput("Logout from app", "Logout sucessful", "App logsout successful", "Fail");
			return false;
		}

		return true;
	}

	public boolean LoginUI() {
		System.out.println("Into Login screen");
		if (objCommon.fGuiClick(ALREADYHAVEACCOUNTBTN) == false) {
			return false;
		}
		System.out.println("Identifying UI components of Login screen");
		// Already have an account text
		if (objCommon.fGuiIsDisplayed(AlreadyhaveanaccountText) == false) {
			return false;
		}

		// Enter password text
		if (objCommon.fGuiIsDisplayed(Enterpassword) == false) {
			return false;
		}
		// Login button
		if (objCommon.fGuiIsDisplayed(LOGINBTN) == false) {
			return false;
		}

		return true;
	}

	public boolean LoginError() {

		System.out.println("Error while clicking Create Account button without adding any input");
		// Click LOGIN button
		if (objCommon.fGuiClick(LOGINBTN) == false) {
			return false;
		}
		System.out.println("Remains on Login screen only");
		if (objCommon.fGuiIsDisplayed(AlreadyhaveanaccountText) == false) {
			return false;
		}

		System.out.println("Error while login with invalid credentials");
		// Click and enter email name
		if (objCommon.fGuiClick(Enteremail) == false) {
			return false;
		}
		if (objCommon.fGuiEnterText(Enteremail, Dictionary.get("EMAILLOGIN")) == false) {
			return false;
		}
		// Hide keyboard
		if (objCommon.fGuiHideKeyBoard() == false) {
			return false;
		}
		// Click and enter password
		if (objCommon.fGuiClick(Enterpassword) == false) {
			return false;
		}
		if (objCommon.fGuiEnterText(Enterpassword, Dictionary.get("REGINVALIDPASSWORD")) == false) {
			return false;
		}
		// Hide keyboard
		if (objCommon.fGuiHideKeyBoard() == false) {
			return false;
		}
		// Click LOGIN button
		if (objCommon.fGuiClick(LOGINBTN) == false) {
			return false;
		}
		System.out.println("Remains on Login screen only");
		if (objCommon.fGuiIsDisplayed(AlreadyhaveanaccountText) == false) {
			return false;
		}
		return true;
	}

	public boolean SuccessfulLogin() {

		System.out.println("Fill up the login fields with the valid credentials");

		// Click and enter email name
		if (objCommon.fGuiClick(Enteremail) == false) {
			return false;
		}
		if (objCommon.fGuiEnterText(Enteremail, Dictionary.get("EMAILLOGIN")) == false) {
			return false;
		}
		// Hide keyboard
		if (objCommon.fGuiHideKeyBoard() == false) {
			return false;
		}
		// Click and enter password
		if (objCommon.fGuiClick(Enterpassword) == false) {
			return false;
		}
		if (objCommon.fGuiEnterText(Enterpassword, Dictionary.get("PASSWORDLOGIN")) == false) {
			return false;
		}
		// Hide keyboard
		if (objCommon.fGuiHideKeyBoard() == false) {
			return false;
		}
		// Click LOGIN button
		if (objCommon.fGuiClick(LOGINBTN) == false) {
			return false;
		}
		try {
			Thread.sleep(7000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.print("Into home screen");
		// Dashboard text on home screen
		if (objCommon.fGuiClick(DASHBOARD) == false) {
			return false;
		}

		return true;
	}

	public boolean Logout() {

		if (objCommon.fGuiClick(MoreOption) == false) {
			return false;
		}

		if (objCommon.fGuiClick(Logout) == false) {
			return false;
		}

		return true;
	}

}