package com.rohit.asap;

import static org.testng.Assert.assertEquals;

import java.sql.Timestamp;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ByName;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.mobile.NetworkConnection;
import org.openqa.selenium.mobile.NetworkConnection.ConnectionType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import io.appium.java_client.MobileBy;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.*;

public class CommonFunctions {

	private static final String AppiumDriver = null;
	// *************************************************************
	/** List of Methods in this class **/
	// ** boolean fGuiIsDisplayed (String strDesc)
	// ** boolean fGuiIsNotDisplayed (String strDesc)
	// ** boolean fGuiClick (String strDesc)
	// ** boolean fGuiEnterText (String strDesc, String strText)
	// ** WebElement getObject(String strDesc)
	// ** boolean fGuiHideKeyBoard(NIL)
	// ** boolean fGuiWait(NIL)

	/** List of Methods in this class **/
	// *************************************************************

	// Properties
	private Reporting Reporter;
	static WebDriver driver;
	private Driver asapDriver = new Driver();

	// Constructor
	public CommonFunctions() {

		// Create the object of the Reporter
		this.Reporter = Global.Reporter;

		if (asapDriver.fGetPlatform().equals("Android")) {
			this.driver = Global.androidDriver;

		} else
			this.driver = Global.iosDriver;
	}

	// *****************************************************************************************
	// * Name : fGuiIsDisplayed
	// * Description : Check if the object is displayed or not as per the choice
	// * Input Params : strDesc(The property of the object)
	// * Return Values : Boolean - Depending on the success
	// *****************************************************************************************
	public synchronized boolean fGuiIsDisplayed(String strDesc) {
		try {

			// Get WebElement
			WebElement webElement = getObject(strDesc);

			// Check if the WebElement is displayed
			boolean bIsDisplayed = false;

			int intCount = 1;

			// Loop for around 10 secs to check whether object is being
			// displayed
			while (!(bIsDisplayed) && (intCount <= 10)) {
				try {
					bIsDisplayed = webElement.isDisplayed();

				} catch (Exception e) {
					Reporter.fnWriteToHtmlOutput("Check if element with description  " + strDesc + " is displayed",
							"Exception occurred", "Exception :" + e, "Fail");
					e.printStackTrace();
					return false;
				}

				// Sleep for a sec
				Thread.sleep(200);
				intCount++;
			}

			// Validate if the element should be displayed or not
			if (bIsDisplayed) {
				Reporter.fnWriteToHtmlOutput("Check if object with description  " + strDesc + " is displayed",
						"Object should be displayed", "Object is Displayed", "Pass");
				return true;
			} else {
				Reporter.fnWriteToHtmlOutput("Check if object with description  " + strDesc + " is displayed",
						"Object should be displayed", "Object is not displayed", "Fail");
				return false;
			}

		} catch (Exception e) {
			Reporter.fnWriteToHtmlOutput("Check if element with description  " + strDesc + " is displayed",
					"Exception occurred", "Exception :" + e, "Fail");
			e.printStackTrace();
			return false;
		}
	}

	// *****************************************************************************************
	// * Name : fGuiIsDisplayed
	// * Description : Check if the object is displayed or not as per the choice
	// * Input Params : Webelement
	// * Return Values : Boolean - Depending on the success
	// *****************************************************************************************
	public boolean fGuiIsDisplayed(WebElement webElement) {

		// Check if the WebElement is displayed
		boolean bIsDisplayed = false;
		String strDesc = webElement.toString();

		int intCount = 1;

		try {

			// Loop for around 10 secs to check whether object is being
			// displayed
			while (!(bIsDisplayed) && (intCount <= 10)) {
				try {
					bIsDisplayed = webElement.isDisplayed();

				} catch (Exception e) {
					Reporter.fnWriteToHtmlOutput("Check if element with description  " + strDesc + " is displayed",
							"Exception occurred", "Exception :" + e, "Fail");
					e.printStackTrace();
					return false;
				}

				// Sleep for a sec
				Thread.sleep(1000);
				intCount++;
			}

			// Validate if the element should be displayed or not
			if (bIsDisplayed) {
				Reporter.fnWriteToHtmlOutput("Check if object with description  " + strDesc + " is displayed",
						"Object should be displayed", "Object is Displayed", "Pass");
				return true;
			} else {
				Reporter.fnWriteToHtmlOutput("Check if object with description  " + strDesc + " is displayed",
						"Object should be displayed", "Object is not displayed", "Fail");
				return false;
			}

		} catch (Exception e) {
			Reporter.fnWriteToHtmlOutput("Check if element with description  " + strDesc + " is displayed",
					"Exception occurred", "Exception :" + e, "Fail");
			e.printStackTrace();
			return false;
		}
	}

	// *****************************************************************************************
	// * Name : fGuiIsNotDisplayed
	// * Description : Check if the object is displayed or not as per the choice
	// * Input Params : strDesc(The property of the object),
	// choiceYesOrNo(displayed->Yes; notDisplayed->No)
	// * Return Values : Boolean - Depending on the success
	// *****************************************************************************************
	public boolean fGuiIsNotDisplayed(String strDesc) {
		try {

			// Get WebElement
			WebElement webElement = getObject(strDesc);

			// Check if the WebElement is displayed
			boolean bNotIsDisplayed = false;

			int intCount = 1;

			// Loop for around 10 secs to check whether object is being
			// displayed
			while (!(bNotIsDisplayed) && (intCount <= 10)) {
				try {
					bNotIsDisplayed = !(webElement.isDisplayed());

				} catch (Exception e) {
					Reporter.fnWriteToHtmlOutput("Check if element with description  " + strDesc + " is not displayed",
							"Exception occurred", "Exception :" + e, "Fail");
					e.printStackTrace();
					return false;
				}

				// Sleep for a sec
				Thread.sleep(1000);
				intCount++;
			}

			// Validate if the element should be displayed or not
			if (bNotIsDisplayed) {
				Reporter.fnWriteToHtmlOutput("Check if object with description  " + strDesc + " is not displayed",
						"Object should not be displayed", "Object is not displayed", "Pass");
				return true;
			} else {
				Reporter.fnWriteToHtmlOutput("Check if object with description  " + strDesc + " is not displayed",
						"Object should not be displayed", "Object is Displayed", "Fail");
				return false;
			}

		} catch (Exception e) {
			Reporter.fnWriteToHtmlOutput("Check if element with description  " + strDesc + " is displayed",
					"Exception occurred", "Exception :" + e, "Fail");
			e.printStackTrace();
			return false;
		}
	}

	// *****************************************************************************************
	// * Name : fGuiIsNotDisplayed
	// * Description : Check if the object is displayed or not as per the choice
	// * Input Params : WebElement
	// * Return Values : Boolean - Depending on the success
	// *****************************************************************************************
	public boolean fGuiIsNotDisplayed(WebElement webElement) {

		// Check if the WebElement is displayed
		boolean bNotIsDisplayed = false;
		String strDesc = webElement.toString();

		// Counter
		int intCount = 1;

		try {
			// Loop for around 10 secs to check whether object is being
			// displayed
			while (!(bNotIsDisplayed) && (intCount <= 10)) {
				try {
					bNotIsDisplayed = !(webElement.isDisplayed());

				} catch (Exception e) {
					Reporter.fnWriteToHtmlOutput("Check if element with description  " + strDesc + " is not displayed",
							"Exception occurred", "Exception :" + e, "Fail");
					e.printStackTrace();
					return false;
				}

				// Sleep for a sec
				Thread.sleep(1000);
				intCount++;
			}

			// Validate if the element should be displayed or not
			if (bNotIsDisplayed) {
				Reporter.fnWriteToHtmlOutput("Check if object with description  " + strDesc + " is not displayed",
						"Object should not be displayed", "Object is not displayed", "Pass");
				return true;
			} else {
				Reporter.fnWriteToHtmlOutput("Check if object with description  " + strDesc + " is not displayed",
						"Object should not be displayed", "Object is Displayed", "Fail");
				return false;
			}

		} catch (Exception e) {
			Reporter.fnWriteToHtmlOutput("Check if element with description  " + strDesc + " is displayed",
					"Exception occurred", "Exception :" + e, "Fail");
			e.printStackTrace();
			return false;
		}
	}

	// *****************************************************************************************
	// * Name : fGuiClick
	// * Description : Click on the passed object
	// * Input Params : strDesc - The description of the object to click
	// * Return Values : Boolean - Depending on the success
	// *****************************************************************************************
	public synchronized boolean fGuiClick(String strDesc) {
		try {
			// Initialize
			WebElement objClick;

			// Call the function to get the webelement based on the description
			objClick = getObject(strDesc);

			// if null is returned
			if (objClick == null)
				return false;

			// Check if the object is enabled, if yes click the same
			if (objClick.isEnabled()) {
				// Click on the object
				objClick.click();
			} else {
				Reporter.fnWriteToHtmlOutput("Check if object is enabled " + strDesc,
						"Object with description " + strDesc + " should be enabled", "Object is not enabled", "Fail");
				return false;
			}

			Reporter.fnWriteToHtmlOutput("Click object matching description " + strDesc,
					"Click operation should be successful", "Successfully clicked the object", "Done");
			return true;

		} catch (Exception e) {
			e.printStackTrace();
			Reporter.fnWriteToHtmlOutput("Click object matching description " + strDesc, "Click operation failed",
					"Exception occured while click object", "Fail");
			return false;
		}
	}

	// *****************************************************************************************
	// * Name : fGuiClick
	// * Description : Click on the passed object
	// * Input Params : strDesc - The description of the object to click
	// * Return Values : Boolean - Depending on the success
	// *****************************************************************************************
	public synchronized boolean fGuiClick(WebElement objClick) {
		try {

			// Check if the object is enabled, if yes click the same
			if (objClick.isEnabled()) {
				// Click on the object
				objClick.click();

			} else {
				// Reporter.fnWriteToHtmlOutput("Check if object is enabled " +
				// objClick.toString(), "Object with description " +
				// objClick.toString() + " should be enabled",
				// "Object is not enabled", "Fail");
				return false;
			}

			// Reporter.fnWriteToHtmlOutput("Click object matching description "
			// + objClick.toString(), "Click operation should be successful",
			// "Successfully clicked the object", "Done");
			return true;

		} catch (Exception e) {
			e.printStackTrace();
			Reporter.fnWriteToHtmlOutput("Click object matching description " + objClick.toString(),
					"Click operation failed", "Exception occured while click object", "Fail");
			return false;
		}
	}

	// *****************************************************************************************
	// * Name : fGuiEnterText
	// * Description : Click on the passed object
	// * Input Params : WebElement - The webelement to click, strText = Text to
	// enter
	// * Return Values : Boolean - Depending on the success
	// *****************************************************************************************
	public synchronized boolean fGuiEnterText(String strDesc, String strText) {
		try {
			WebElement objEdit;

			// Call the function to get the webelement based on the description
			objEdit = getObject(strDesc);

			// if null is returned
			if (objEdit == null)
				return false;

			// Check if the object is enabled, if yes click the same
			if (objEdit.isEnabled()) {
				// Enter the text in the edit box
				objEdit.clear();
				objEdit.sendKeys(strText);

			} else {
				Reporter.fnWriteToHtmlOutput("Check if object is enabled " + strDesc,
						"Object with description " + strDesc + " should be enabled", "Object is not enabled", "Fail");
				return false;
			}

			Reporter.fnWriteToHtmlOutput("Set value in object with description " + strDesc,
					"Value " + strText + " should be set in the edit box", "Value is set in the text field", "Done");
			return true;
		} catch (Exception e) {
			Reporter.fnWriteToHtmlOutput("Set value in object with description " + strDesc,
					"Value " + strText + " should be set in the edit box",
					"Exception " + e + " occured while setting the value", "Fail");
			return false;
		}

	}

	// *****************************************************************************************
	// * Name : fGuiEnterText
	// * Description : Click on the passed object
	// * Input Params : WebElement - The webelement to click, strText = Text to
	// enter
	// * Return Values : Boolean - Depending on the success
	// *****************************************************************************************
	public synchronized boolean fGuiEnterText(WebElement objEdit, String strText) {

		String strDesc = objEdit.toString();
		try {
			// if null is returned
			if (objEdit == null)
				return false;

			// Check if the object is enabled, if yes click the same
			if (objEdit.isEnabled()) {
				// Enter the text in the edit box
				objEdit.sendKeys(strText);
			} else {
				Reporter.fnWriteToHtmlOutput("Check if object is enabled " + strDesc,
						"Object with description " + strDesc + " should be enabled", "Object is not enabled", "Fail");
				return false;
			}

			Reporter.fnWriteToHtmlOutput("Set value in object with description " + strDesc,
					"Value " + strText + " should be set in the edit box", "Value is set in the text field", "Done");
			return true;
		} catch (Exception e) {
			Reporter.fnWriteToHtmlOutput("Set value in object with description " + strDesc,
					"Value " + strText + " should be set in the edit box",
					"Exception " + e + " occured while setting the value", "Fail");
			return false;
		}

	}

	// *****************************************************************************************
	// * Name : getObject
	// * Description : Returns the webelement based on the description
	// * Input Params : objDesc - Description of the object
	// * Return Values : Webelement - Webelement based on the description
	// *****************************************************************************************
	public WebElement getObject(String objDesc) {
		// Delimiters
		String[] delimiters = new String[] { ":=" };
		String[] arrFindByValues = objDesc.split(delimiters[0]);

		// Get Findby and Value
		String FindBy = arrFindByValues[0];
		String val = arrFindByValues[1];

		try {
			// Handle all FindBy cases
			String strElement = FindBy.toLowerCase();
			if (strElement.equalsIgnoreCase("linktext")) {
				return driver.findElement(By.linkText(val));
			} else if (strElement.equalsIgnoreCase("partiallinktext")) {
				return driver.findElement(By.partialLinkText(val));
			} else if (strElement.equalsIgnoreCase("xpath")) {
				return driver.findElement(By.xpath(val));
			} else if (strElement.equalsIgnoreCase("name")) {
				return driver.findElement(By.name(val));
			} else if (strElement.equalsIgnoreCase("id")) {
				return driver.findElement(By.id(val));
			} else if (strElement.equalsIgnoreCase("classname")) {
				return driver.findElement(By.className(val));
			} else if (strElement.equalsIgnoreCase("cssselector")) {
				return driver.findElement(By.cssSelector(val));
			} else if (strElement.equalsIgnoreCase("AndroidUIAutomator")) {
				return driver.findElement(By.tagName(val));
			} else if (strElement.equalsIgnoreCase("tagname")) {
				return driver.findElement(By.tagName(val));
			} else if (strElement.equalsIgnoreCase("accessibility_id")) {
				return (driver).findElement(By.id(val));
			} else if (strElement.equalsIgnoreCase("appclassname")) {
				return (driver).findElement(By.className(val));
			} else {
				Reporter.fnWriteToHtmlOutput("Get object matching description " + objDesc,
						"Object should be found and returned",
						"Property " + FindBy + " specified for object is invalid", "Fail");
				System.out.println("Property name " + FindBy + " specified for object " + objDesc + " is invalid");
				return null;
			}

		}

		// Catch Block
		catch (Exception e) {
			Reporter.fnWriteToHtmlOutput("Get object matching description " + objDesc,
					"Object should be found and returned", "Unable to find required object", "Fail");
			System.out.println("Exception " + e.toString() + " occured while fetching the object");
			return null;
		}

	}

	// *****************************************************************************************
	// * Name : fGuiHideKeyBoard()
	// * Description : hides the keyboard
	// * Input Params : nil
	// * Return Values : Boolean - Depending on the success
	// ***************
	public synchronized boolean fGuiHideKeyBoard() {
		((AndroidDriver) driver).hideKeyboard();
		return true;

	}

	// *****************************************************************************************
	// * Name : fGuiWait()
	// * Description : Wait
	// * Input Params : nil
	// * Return Values : Boolean - Depending on the success
	// ***************
	public synchronized boolean fGuiWait() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		return true;

	}

}
